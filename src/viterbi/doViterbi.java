package viterbi;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Stack;

import java.util.TreeMap;

public class doViterbi {
	  private static Character[] sequence;
	  public static MarkovChain chain;
	  public static Double[][] vTable;
	  public static Integer[][] ptrTable;
	  public static Integer startInt;
	  public static Integer endInt;
	  
	  public static void main(String[] args) {
	    // Make sure an input file was specified on the command line.
	    // If this fails and you are using Eclipse, see the comments 
	    // in the header above.
	    if (args.length != 5 ) {
	      System.err.println("Please supply a filename on the " +
				 "command line: java doViterbi ");
	      System.exit(1);
	    }
	    
	    // Try creating a scanner to read the input file.
	    Scanner transitionFileScanner = null;
	    try {
	      transitionFileScanner = new Scanner(new File(args[0]));
	    } catch(FileNotFoundException e) {
	      System.err.println("Could not find file '" + args[0] + 
				 "'.");
	      System.exit(1);
	    }
	    
	    Scanner emissionFileScanner = null;
	    try {
	      emissionFileScanner = new Scanner(new File(args[1]));
	    } catch(FileNotFoundException e) {
	      System.err.println("Could not find file '" + args[1] + 
				 "'.");
	      System.exit(1);
	    }
	    
	    sequence = new Character[args[4].length()+1];
	    
	    for(int i=0; i< args[4].length(); i++){
	    	sequence[i+1] = args[4].charAt(i);
	    }

	    
	    // Create a DatasetParser, parse the datasets
	    DatasetParser parser = new DatasetParser();
	    //Initialize Chain
	    startInt = Integer.parseInt(args[2]);
	    endInt = Integer.parseInt(args[3]);

	    chain = new MarkovChain();
	    
	    //Build the chain from the files
	    parser.parseTransitionFile(transitionFileScanner);   
	    parser.parseEmissionFile(emissionFileScanner);
	    
	    vTable = new Double[sequence.length + 1][(endInt-startInt) + 1];
	    ptrTable = new Integer[sequence.length + 1][(endInt-startInt) + 1];
	    for(int i =0; i<vTable[0].length; i++){
	    	vTable[0][i] = 0.0;
	    }
	    for(int i =0; i<vTable.length; i++){
	    	vTable[i][0] = 0.0;
	    }
	    vTable[0][0] = 1.0;
	    
	    Double probability = Viterbi(1);
	    System.out.println("Viterbi Probability=" +  probability.floatValue());
	    printPath();
	  }
	  
	  
	  /*
	   * Public method to compute the cost of a tree. uses helper
	   * method to do recursion
	   */
	  
	  public static Double Viterbi(Integer T){
		  //Loop through all rows k for this T
		  
		  for( int k =0; k < vTable[T].length; k++){
			  if(chain.getNode(k)==null){
				  //Continue if it is not the termination step
				  if(T != sequence.length -1 || k != vTable[T].length-1){
					  vTable[T][k]=0.0;
					  continue;
				  }
			  }
			  Double probability;
			  //Multiply emission probability by the max of valid transitions
			  
			  
			  
			  Integer maxIndex = 0;
			  Double max = 0.0;
			  
			  //Termination
			  if(chain.getNode(k) == null){
				 probability=1.0;//if it is the temination step
				 ArrayList<Integer> lIndex = new ArrayList<Integer>();
				  //first find valid transitions to specific k
				  for(int i = 0; i < chain.transitions.length; i++){
					  if(chain.transitions[i][k] != null){
						  lIndex.add(i);
					  }
				  }
				  Iterator<Integer> i = lIndex.iterator();
				  while(i.hasNext()){
					  Integer curr = i.next();
					  Double currValue = vTable[T][curr]*chain.transitions[curr][k];
					  if(currValue == max) maxIndex = curr;
					  if(currValue> max){
						  maxIndex =curr;
						  max = currValue;
					  }  
				  }
				  probability *= max;
			  }
			  
			  //All other steps
			  else{
			  probability = chain.getNode(k).get(sequence[T]);
			  ArrayList<Integer> lIndex = new ArrayList<Integer>();
			  //first find valid transitions to specific k
			  for(int i = 0; i < chain.transitions.length; i++){
				  if(chain.transitions[i][k] != null){
					  lIndex.add(i);
				  }
			  }
			  //Iterate through transitions to generate values to find maximum of
			  Iterator<Integer> i = lIndex.iterator();
			  while(i.hasNext()){
				  Integer curr = i.next();
				  Double currValue = vTable[T-1][curr]*chain.transitions[curr][k];
				  if(currValue == max) maxIndex = curr;
				  if(currValue> max){
					  maxIndex =curr;
					  max = currValue;
				  }  
			  }
			  probability *= max;
			  }
			  
			  System.out.println( "At T=" + T +  ", k=" + k + " Viterbi="+ probability.floatValue() + "  Max State = "+ maxIndex);
			  ptrTable[T][k] = maxIndex;
			  vTable[T][k] = probability;
			  
		  }
		  if(T+1 > sequence.length-1) return vTable[T][endInt];
	      return Viterbi(T+1);
	  }
	  
	  
	  public static void printPath(){
		  System.out.println("\nTraceback:");
		  Stack<String> s = new Stack<String>();
		  int back = ptrTable[sequence.length-1][vTable[0].length-1];
		  int t = sequence.length -1;
		  while(t >= 0){
			  s.push(back + " -> " + sequence[t]);
			  if(sequence[t] == null)break;
			  back = ptrTable[t][back];
			  t--;
		  }
		  while(!s.empty()){
			  System.out.println(s.pop());
		  }

	  }
		
	}//end class

	
	

	class MarkovChain{
		private TreeMap<Integer, TreeMap<Character, Double>> nodes;
		public Double[][] transitions;
		public void addNode(Integer i){
			TreeMap<Character, Double> t = new TreeMap<Character, Double>();
			this.nodes.put(i, t);
		}
		
		public boolean nodeExists(Integer i){
			return this.nodes.containsKey(i);
		}
		
		public TreeMap<Character, Double> getNode(Integer i){
			return this.nodes.get(i);
		}
		
		public MarkovChain(){
			nodes= new TreeMap<Integer, TreeMap<Character, Double>>();
			transitions = new Double[100][100];
		}
	}

	/**
	 * This class is responsible for parsing datasets in specific format.
	 * Builds a tree from the tree file, puts values into the tree with the
	 * assign file and creates the score matrix with the score file.
	 */
	class DatasetParser {
	  public DatasetParser() {}
	  

	public void parseEmissionFile(Scanner emissionFileScanner) {
		while(emissionFileScanner.hasNextLine()) {
		      String line = emissionFileScanner.nextLine().trim();
			  if(line == null) continue;
		      String[] l = line.split("\\s+");
		      Integer index = Integer.parseInt(l[0]);
		      Character base = l[1].toCharArray()[0];
		      Double value = Double.parseDouble(l[2]);
		      if(!doViterbi.chain.nodeExists(index)){
		    	  doViterbi.chain.addNode(index);
		      }
		      doViterbi.chain.getNode(index).put(base, value);
		  }
	}



	public void parseTransitionFile(Scanner transitionFileScanner) {
		while(transitionFileScanner.hasNextLine()) {
		      String line = transitionFileScanner.nextLine().trim();
			  if(line == null) continue;
		      String[] l = line.split("\\s+");
		      Integer start = Integer.parseInt(l[0]);
		      Integer end =  Integer.parseInt(l[1]);
		      doViterbi.chain.transitions[start][end] = Double.parseDouble(l[2]);
		  }
	}



	/**
	   * Returns the first token encountered on a significant line in the file.
	   *
	   * @param fileScanner a Scanner used to read the file.
	   */
	  private String parseSingleToken(Scanner fileScanner) {
	    String line = findSignificantLine(fileScanner);

	    // Once we find a significant line, parse the first token on the
	    // line and return it.
	    Scanner lineScanner = new Scanner(line);
	    return lineScanner.next();
	  }


	  /**
	   * Returns the next line in the file which is significant (i.e., is not
	   * all whitespace or a comment).
	   *
	   * @param fileScanner a Scanner used to read the file
	   */
	  private String findSignificantLine(Scanner fileScanner) {
	    // Keep scanning lines until we find a significant one.
	    while(fileScanner.hasNextLine()) {
	      String line = fileScanner.nextLine().trim();
	      if (isLineSignificant(line)) {
		return line;
	      }
	    }

	    // If the file is in proper format, this should never happen.
	    return null;
	  }

	  /**
	   * Returns whether the given line is significant (i.e., not blank or a
	   * comment). The line should be trimmed before calling this.
	   *
	   * @param line the line to check
	   */
	  private boolean isLineSignificant(String line) {
	    line = line.trim(); // Do this again for robustness in case we ever call this from other code.  It is a fast calculation.
	    // Blank lines are not significant.
	    if(line == null || line.length() == 0) { // Should never get null, but still good idea to check.
	      return false;
	    }

	    // Lines which have consecutive forward slashes as their first two
	    // characters are comments and are not significant.
	    if(line.startsWith("//")) {
	      return false;
	    }

	    return true;
	  }
	
	
}
